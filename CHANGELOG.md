## [1.2.8](https://gitlab.com/banerjeesouvik/ci-cd-test/compare/v1.2.7...v1.2.8) (2020-07-03)


### Bug Fixes

* keep node_modules as artifacts but not as cache ([d31ff1d](https://gitlab.com/banerjeesouvik/ci-cd-test/commit/d31ff1d48b7a2da95d6a8854f9b359e17e161a18))
* remove additional assets from gitlab plugin ([303c141](https://gitlab.com/banerjeesouvik/ci-cd-test/commit/303c1412f4a1462ad7ca41d8d8c0f5cb5e912303))

## [1.2.7](https://gitlab.com/banerjeesouvik/ci-cd-test/compare/v1.2.6...v1.2.7) (2020-07-02)


### Bug Fixes

* change asset path ([c6dd57f](https://gitlab.com/banerjeesouvik/ci-cd-test/commit/c6dd57fcc9c16fa1256995d2e46256a80103ad33))

## [1.2.6](https://gitlab.com/banerjeesouvik/ci-cd-test/compare/v1.2.5...v1.2.6) (2020-07-02)


### Bug Fixes

* add custom asset to gitlab release ([fcf1ccc](https://gitlab.com/banerjeesouvik/ci-cd-test/commit/fcf1ccc11e661bcdd4cb61d55a7a202e4691a1c3))
* ci file invalid key ([13d97cc](https://gitlab.com/banerjeesouvik/ci-cd-test/commit/13d97cc4b6d331ac4af1a76b16e8fd6dc15939d2))

## [1.2.5](https://gitlab.com/banerjeesouvik/ci-cd-test/compare/v1.2.4...v1.2.5) (2020-07-02)


### Bug Fixes

* add global cache instead of local cache to share node_modules ([dc9bca9](https://gitlab.com/banerjeesouvik/ci-cd-test/commit/dc9bca970b426bb1bb926c45950748de86e9b708))
* break ci steps in two - install and release ([df5e438](https://gitlab.com/banerjeesouvik/ci-cd-test/commit/df5e43821a0fa231646037a7d84a605ead504fc5))
* cache node_modules between stages ([d7ddb0f](https://gitlab.com/banerjeesouvik/ci-cd-test/commit/d7ddb0f8bc91c9412dab21a26dc565b2de97b613))

## [1.2.4](https://gitlab.com/banerjeesouvik/ci-cd-test/compare/v1.2.3...v1.2.4) (2020-07-02)


### Bug Fixes

* fix email subject template and MR links ([3da6ffe](https://gitlab.com/banerjeesouvik/ci-cd-test/commit/3da6ffeceda91b39ba1eef7ed1fbfca1fbe473f1))

## [1.2.3](https://gitlab.com/banerjeesouvik/ci-cd-test/compare/v1.2.2...v1.2.3) (2020-07-02)


### Bug Fixes

* downgradre sendgrid to v6 ([3c23805](https://gitlab.com/banerjeesouvik/ci-cd-test/commit/3c2380596e71c48e682aec691a0ef81df8e1c78e))

## [1.2.2](https://gitlab.com/banerjeesouvik/ci-cd-test/compare/v1.2.1...v1.2.2) (2020-07-02)


### Bug Fixes

* filter releaseNotes from CLAs ([3ad7586](https://gitlab.com/banerjeesouvik/ci-cd-test/commit/3ad75861ebbf73623f4ff519b552cd9f464401a7))

## [1.2.1](https://gitlab.com/banerjeesouvik/ci-cd-test/compare/v1.2.0...v1.2.1) (2020-07-02)


### Bug Fixes

* enclose notes in quotes before passing as command line argument ([88fa309](https://gitlab.com/banerjeesouvik/ci-cd-test/commit/88fa30955fbbb468d7a64bd8dca56ae969a177b9))

# [1.2.0](https://gitlab.com/banerjeesouvik/ci-cd-test/compare/v1.1.1...v1.2.0) (2020-07-02)


### Features

* notify user dummy  console ([d525927](https://gitlab.com/banerjeesouvik/ci-cd-test/commit/d525927ebfff68d283b1c15891d910c5be0f3c7d))

## [1.1.1](https://gitlab.com/banerjeesouvik/ci-cd-test/compare/v1.1.0...v1.1.1) (2020-07-02)


### Bug Fixes

* add npm plugin and make repo private ([fda6124](https://gitlab.com/banerjeesouvik/ci-cd-test/commit/fda61246ba6fae6b2036b0da80b271d0c096524b))

# [1.1.0](https://gitlab.com/banerjeesouvik/ci-cd-test/compare/v1.0.0...v1.1.0) (2020-06-24)


### Features

* add semantic-release/git plugin to commit changelogs ([749f967](https://gitlab.com/banerjeesouvik/ci-cd-test/commit/749f967284d61c6c8127e1819acdc84b3b18cd92))
