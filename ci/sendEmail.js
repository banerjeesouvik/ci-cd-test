const sgMail = require('@sendgrid/mail')

const CLA = process.argv
const sgApi = process.env.SENDGRID_API_KEY
const projectName = process.env.CI_PROJECT_NAME

const getCLA = (argName) => CLA.filter(cla => cla.startsWith(argName))[0].replace(argName, '')

sgMail.setApiKey(sgApi);

const releaseNotes = getCLA('releaseNotes=')
const version = getCLA('version=')

const msg = {
  to: 'banerjeesouvik018@gmail.com',
  from: 'souvik@zopsmart.com',
  subject: `New release ${version} for ${projectName}`,
  text: releaseNotes,
};

sgMail
  .send(msg)
  .then(() => {
    console.log('Users are notified about the release')
  },
  error => {
    console.log('Unable to notify users about the new release')
    console.error(error);

    if (error.response) {
      console.error(error.response.body)
    }
  });
